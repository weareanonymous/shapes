const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: './js/index.js',
  output: {
    filename: './dist/bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.less$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
            },
            { loader: 'postcss-loader' },
            {
              loader: 'less-loader',
            },
          ],
          fallback: 'style-loader',
        }),
      },
    ],
  },
  plugins: [new ExtractTextPlugin('./dist/styles.css')],
};
