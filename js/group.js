import { CIRCLE, MATH } from './helper';

class Group {
  constructor(parentEl, Circle, Parallelogram) {
    this.parentElement = parentEl;
    this.circleClass = Circle;
    this.parallelogramClass = Parallelogram;
    this.points = [];
    this.parallelogram = null;
    this.circle = null;
    this.activePoint = null;
  }
  addPoint(x, y) {
    if (this.points.length <= 3) {
      this._createPoint(x, y);
    }
    if (this.points.length === 3) {
      const { x, y } = MATH.calc4thPoint(this.points, 1);
      this._createPoint(x, y);
      this._createParallelogram();
      this._createCircle();
    }
  }
  mouseDown(x, y) {
    const point = this._findPoint(x, y);
    this.activePoint = point === -1 ? null : point;
  }
  mouseUp() {
    this.activePoint = null;
  }
  mouseMove(x, y) {
    if (this.activePoint === null) return false;
    this._updateActivePoint(x, y);
    this._updateSymmetricPoint();
    this._updateParallelogram();
    this._updateCircle();
  }
  _createPoint(x, y) {
    const point = new this.circleClass(x, y);
    this.points.push(point);
    this.parentElement.prepend(point.element);
  }
  _createParallelogram() {
    const parallelogram = new this.parallelogramClass(this.points);
    this.parallelogram = parallelogram;
    this.parentElement.prepend(parallelogram.element);
  }
  _createCircle() {
    const { x, y } = MATH.calcCenter(this.points);
    const radius = MATH.calcRadius(this.points);
    const circle = new this.circleClass(x, y, radius);
    this.circle = circle;
    this.parentElement.prepend(circle.element);
  }
  _updateCircle() {
    const { x, y } = MATH.calcCenter(this.points);
    const radius = MATH.calcRadius(this.points);
    this.circle.update({ x, y, radius });
  }
  _updateParallelogram() {
    this.parallelogram.update(this.points);
  }
  _updateActivePoint(x, y) {
    this.points[this.activePoint].update({ x, y });
  }
  _updateSymmetricPoint() {
    const { x, y, symmetricPoint } = MATH.calcSymmetricPoint(
      this.points,
      this.activePoint
    );
    this.points[symmetricPoint].update({ x, y });
  }
  _findPoint(x, y) {
    const point = this.points.findIndex(
      point =>
        point.x > x - CIRCLE.radius &&
        point.x < x + CIRCLE.radius &&
        point.y > y - CIRCLE.radius &&
        point.y < y + CIRCLE.radius
    );
    return point;
  }
  get isCompleted() {
    return this.points.length === 4;
  }
}

export default Group;
