import { CIRCLE, TEXT } from './helper';

class Point {
  constructor(x, y, radius) {
    this.x = x;
    this.y = y;
    this.r = radius >= 0 ? radius : null;
    this.el = null;
    this.circle = null;
    this.label = null;
    this._createElement();
  }
  _createLabel() {
    const position = this.r ? 'translate(-10 0)' : 'translate(10 10)';
    const label = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'text'
    );
    label.setAttribute('transform', position);
    label.setAttribute('fill', TEXT.fill.color.default);
    this.label = label;
    this.el.appendChild(label);
  }
  _createCircle() {
    const circle = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'circle'
    );
    const stroke =
      this.r === null ? CIRCLE.stroke.color.point : CIRCLE.stroke.color.default;
    circle.setAttribute('stroke', stroke);
    circle.setAttribute('stroke-width', CIRCLE.stroke.width);
    circle.setAttribute('fill', 'url(#linearGradient)');
    this.circle = circle;
    this.el.appendChild(circle);
  }
  _createElement() {
    this.el = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this._createLabel();
    this._createCircle();
    this._updateView();
  }
  _updateView() {
    const text = this.r
      ? `Area: ${Math.round(this.r)}`
      : `(${this.x},${this.y})`;
    this.label.innerHTML = text;
    this.circle.setAttribute('r', this.r || CIRCLE.radius);
    this.el.setAttribute('transform', `translate(${this.x},${this.y})`);
  }
  update({ x, y, radius }) {
    this.x = x;
    this.y = y;
    this.r = radius || null;
    this._updateView();
  }
  get element() {
    return this.el;
  }
  get positionX() {
    return this.x;
  }
  get positionY() {
    return this.y;
  }
}

export default Point;
