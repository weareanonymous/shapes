export const CIRCLE = {
  radius: 5.5,
  stroke: {
    color: {
      default: '#ffec07',
      point: '#ff5722',
    },
    width: 3,
  },
};

export const PARALLELOGRAM = {
  stroke: {
    color: {
      default: '#2196f3',
    },
    width: 3,
  },
};

export const TEXT = {
  fill: {
    color: {
      default: '#fff',
    },
  },
};

export const MAP = [
  {
    symmetricPoint: 2,
    pointA: 1,
    pointB: 3,
    pointC: 0,
  },
  {
    symmetricPoint: 3,
    pointA: 0,
    pointB: 2,
    pointC: 1,
  },
  {
    symmetricPoint: 0,
    pointA: 3,
    pointB: 1,
    pointC: 2,
  },
  {
    symmetricPoint: 1,
    pointA: 2,
    pointB: 0,
    pointC: 3,
  },
];

export const MATH = {
  calc4thPoint(points) {
    return this.calcSymmetricPoint(points, 1);
  },
  calcSymmetricPoint(points, activePoint) {
    // index of MAP - an object with rules for an active point
    // pointA, pointB, pointC - points for calculating symmetricPoint
    // symmetricPoint - symmetric point ¯\_(ツ)_/¯
    const { symmetricPoint, pointA, pointB, pointC } = MAP[activePoint];

    const x =
      points[pointA].positionX +
      (points[pointB].positionX - points[pointC].positionX);

    const y =
      points[pointA].positionY +
      (points[pointB].positionY - points[pointC].positionY);

    return {
      x,
      y,
      symmetricPoint,
    };
  },
  calcCenter(points) {
    return {
      x: points[0].x + (points[2].x - points[0].x) / 2,
      y: points[0].y + (points[2].y - points[0].y) / 2,
    };
  },
  calcArea(points) {
    const a = Math.sqrt(
      Math.pow(points[0].x - points[1].x, 2) +
        Math.pow(points[0].y - points[1].y, 2)
    );
    const b = Math.sqrt(
      Math.pow(points[0].x - points[3].x, 2) +
        Math.pow(points[0].y - points[3].y, 2)
    );
    const c = Math.sqrt(
      Math.pow(points[1].x - points[3].x, 2) +
        Math.pow(points[1].y - points[3].y, 2)
    );
    const p = (a + b + c) / 2;

    return 2 * Math.sqrt(p * (p - a) * (p - b) * (p - c));
  },
  calcRadius(points) {
    return Math.sqrt(this.calcArea(points) / Math.PI);
  },
};
