import './../styles/main.less';
import './../styles/canvas.less';
import './../styles/nav.less';
import './../styles/popup.less';
import Canvas from './canvas';
import Group from './group';
import Point from './point';
import Parallelogram from './parallelogram';

const canvasElement = document.querySelector('#canvas');
const canvas = new Canvas(canvasElement, Group, Point, Parallelogram);
canvas.init();

document.querySelector('#reset-button').onclick = e => {
  e.preventDefault();
  canvas.reset();
};
