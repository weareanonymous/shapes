class Canvas {
  constructor(el, Group, Point, Parallelogram) {
    this.el = el;
    this.classGroup = Group;
    this.classPoint = Point;
    this.classParallelogram = Parallelogram;
    this.group = null;
    this._handleClickForAdding = this._handleClickForAdding.bind(this);
    this._handleMouseDown = this._handleMouseDown.bind(this);
    this._handleMouseUp = this._handleMouseUp.bind(this);
    this._handleMouseMove = this._handleMouseMove.bind(this);
  }
  init() {
    this.group = new this.classGroup(
      this.el,
      this.classPoint,
      this.classParallelogram
    );
    this._setControls('FOR_ADDING');
  }
  reset() {
    this.el.innerHTML = '';
    this.init();
  }
  _handleClickForAdding(e) {
    const { x, y } = Canvas.offset(this.el, e.clientX, e.clientY);
    this.group.addPoint(x, y);
    if (this.group.isCompleted) {
      this._removeControls('FOR_ADDING');
      this._setControls('FOR_MOVING');
    }
  }
  _handleMouseDown(e) {
    const { x, y } = Canvas.offset(this.el, e.clientX, e.clientY);
    this.group.mouseDown(x, y);
  }
  _handleMouseUp() {
    this.group.mouseUp();
  }
  _handleMouseMove(e) {
    const { x, y } = Canvas.offset(this.el, e.clientX, e.clientY);
    this.group.mouseMove(x, y);
  }
  _setControls(type) {
    switch (type) {
      case 'FOR_ADDING':
        this.el.addEventListener('click', this._handleClickForAdding);
        break;
      case 'FOR_MOVING':
        this.el.addEventListener('mousedown', this._handleMouseDown);
        this.el.addEventListener('mouseup', this._handleMouseUp);
        this.el.addEventListener('mousemove', this._handleMouseMove);
        break;
    }
  }
  _removeControls(type) {
    switch (type) {
      case 'FOR_ADDING':
        this.el.removeEventListener('click', this._handleClickForAdding);
        break;
      case 'FOR_MOVING':
        this.el.removeEventListener('mousedown', this._handleMouseDown);
        this.el.removeEventListener('mouseup', this._handleMouseUp);
        this.el.removeEventListener('mousemove', this._handleMouseMove);
        break;
    }
  }
  static offset(el, x, y) {
    return {
      x: x - el.getBoundingClientRect().x,
      y: y - el.getBoundingClientRect().y,
    };
  }
}

export default Canvas;
