/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const CIRCLE = {
  radius: 5.5,
  stroke: {
    color: {
      default: '#ffec07',
      point: '#ff5722'
    },
    width: 3
  }
};
/* harmony export (immutable) */ __webpack_exports__["a"] = CIRCLE;


const PARALLELOGRAM = {
  stroke: {
    color: {
      default: '#2196f3'
    },
    width: 3
  }
};
/* harmony export (immutable) */ __webpack_exports__["c"] = PARALLELOGRAM;


const TEXT = {
  fill: {
    color: {
      default: '#fff'
    }
  }
};
/* harmony export (immutable) */ __webpack_exports__["d"] = TEXT;


const MAP = [{
  symmetricPoint: 2,
  pointA: 1,
  pointB: 3,
  pointC: 0
}, {
  symmetricPoint: 3,
  pointA: 0,
  pointB: 2,
  pointC: 1
}, {
  symmetricPoint: 0,
  pointA: 3,
  pointB: 1,
  pointC: 2
}, {
  symmetricPoint: 1,
  pointA: 2,
  pointB: 0,
  pointC: 3
}];
/* unused harmony export MAP */


const MATH = {
  calc4thPoint(points) {
    return this.calcSymmetricPoint(points, 1);
  },
  calcSymmetricPoint(points, activePoint) {
    // index of MAP - an object with rules for an active point
    // pointA, pointB, pointC - points for calculating symmetricPoint
    // symmetricPoint - symmetric point ¯\_(ツ)_/¯
    const { symmetricPoint, pointA, pointB, pointC } = MAP[activePoint];

    const x = points[pointA].positionX + (points[pointB].positionX - points[pointC].positionX);

    const y = points[pointA].positionY + (points[pointB].positionY - points[pointC].positionY);

    return {
      x,
      y,
      symmetricPoint
    };
  },
  calcCenter(points) {
    return {
      x: points[0].x + (points[2].x - points[0].x) / 2,
      y: points[0].y + (points[2].y - points[0].y) / 2
    };
  },
  calcArea(points) {
    const a = Math.sqrt(Math.pow(points[0].x - points[1].x, 2) + Math.pow(points[0].y - points[1].y, 2));
    const b = Math.sqrt(Math.pow(points[0].x - points[3].x, 2) + Math.pow(points[0].y - points[3].y, 2));
    const c = Math.sqrt(Math.pow(points[1].x - points[3].x, 2) + Math.pow(points[1].y - points[3].y, 2));
    const p = (a + b + c) / 2;

    return 2 * Math.sqrt(p * (p - a) * (p - b) * (p - c));
  },
  calcRadius(points) {
    return Math.sqrt(this.calcArea(points) / Math.PI);
  }
};
/* harmony export (immutable) */ __webpack_exports__["b"] = MATH;


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__styles_main_less__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__styles_main_less___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__styles_main_less__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__styles_canvas_less__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__styles_canvas_less___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__styles_canvas_less__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__styles_nav_less__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__styles_nav_less___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__styles_nav_less__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__styles_popup_less__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__styles_popup_less___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__styles_popup_less__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__canvas__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__group__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__point__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__parallelogram__ = __webpack_require__(9);









const canvasElement = document.querySelector('#canvas');
const canvas = new __WEBPACK_IMPORTED_MODULE_4__canvas__["a" /* default */](canvasElement, __WEBPACK_IMPORTED_MODULE_5__group__["a" /* default */], __WEBPACK_IMPORTED_MODULE_6__point__["a" /* default */], __WEBPACK_IMPORTED_MODULE_7__parallelogram__["a" /* default */]);
canvas.init();

document.querySelector('#reset-button').onclick = e => {
  e.preventDefault();
  canvas.reset();
};

/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 3 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 4 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 5 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class Canvas {
  constructor(el, Group, Point, Parallelogram) {
    this.el = el;
    this.classGroup = Group;
    this.classPoint = Point;
    this.classParallelogram = Parallelogram;
    this.group = null;
    this._handleClickForAdding = this._handleClickForAdding.bind(this);
    this._handleMouseDown = this._handleMouseDown.bind(this);
    this._handleMouseUp = this._handleMouseUp.bind(this);
    this._handleMouseMove = this._handleMouseMove.bind(this);
  }
  init() {
    this.group = new this.classGroup(this.el, this.classPoint, this.classParallelogram);
    this._setControls('FOR_ADDING');
  }
  reset() {
    this.el.innerHTML = '';
    this.init();
  }
  _handleClickForAdding(e) {
    const { x, y } = Canvas.offset(this.el, e.clientX, e.clientY);
    this.group.addPoint(x, y);
    if (this.group.isCompleted) {
      this._removeControls('FOR_ADDING');
      this._setControls('FOR_MOVING');
    }
  }
  _handleMouseDown(e) {
    const { x, y } = Canvas.offset(this.el, e.clientX, e.clientY);
    this.group.mouseDown(x, y);
  }
  _handleMouseUp() {
    this.group.mouseUp();
  }
  _handleMouseMove(e) {
    const { x, y } = Canvas.offset(this.el, e.clientX, e.clientY);
    this.group.mouseMove(x, y);
  }
  _setControls(type) {
    switch (type) {
      case 'FOR_ADDING':
        this.el.addEventListener('click', this._handleClickForAdding);
        break;
      case 'FOR_MOVING':
        this.el.addEventListener('mousedown', this._handleMouseDown);
        this.el.addEventListener('mouseup', this._handleMouseUp);
        this.el.addEventListener('mousemove', this._handleMouseMove);
        break;
    }
  }
  _removeControls(type) {
    switch (type) {
      case 'FOR_ADDING':
        this.el.removeEventListener('click', this._handleClickForAdding);
        break;
      case 'FOR_MOVING':
        this.el.removeEventListener('mousedown', this._handleMouseDown);
        this.el.removeEventListener('mouseup', this._handleMouseUp);
        this.el.removeEventListener('mousemove', this._handleMouseMove);
        break;
    }
  }
  static offset(el, x, y) {
    return {
      x: x - el.getBoundingClientRect().x,
      y: y - el.getBoundingClientRect().y
    };
  }
}

/* harmony default export */ __webpack_exports__["a"] = (Canvas);

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__helper__ = __webpack_require__(0);


class Group {
  constructor(parentEl, Circle, Parallelogram) {
    this.parentElement = parentEl;
    this.circleClass = Circle;
    this.parallelogramClass = Parallelogram;
    this.points = [];
    this.parallelogram = null;
    this.circle = null;
    this.activePoint = null;
  }
  addPoint(x, y) {
    if (this.points.length <= 3) {
      this._createPoint(x, y);
    }
    if (this.points.length === 3) {
      const { x, y } = __WEBPACK_IMPORTED_MODULE_0__helper__["b" /* MATH */].calc4thPoint(this.points, 1);
      this._createPoint(x, y);
      this._createParallelogram();
      this._createCircle();
    }
  }
  mouseDown(x, y) {
    const point = this._findPoint(x, y);
    this.activePoint = point === -1 ? null : point;
  }
  mouseUp() {
    this.activePoint = null;
  }
  mouseMove(x, y) {
    if (this.activePoint === null) return false;
    this._updateActivePoint(x, y);
    this._updateSymmetricPoint();
    this._updateParallelogram();
    this._updateCircle();
  }
  _createPoint(x, y) {
    const point = new this.circleClass(x, y);
    this.points.push(point);
    this.parentElement.prepend(point.element);
  }
  _createParallelogram() {
    const parallelogram = new this.parallelogramClass(this.points);
    this.parallelogram = parallelogram;
    this.parentElement.prepend(parallelogram.element);
  }
  _createCircle() {
    const { x, y } = __WEBPACK_IMPORTED_MODULE_0__helper__["b" /* MATH */].calcCenter(this.points);
    const radius = __WEBPACK_IMPORTED_MODULE_0__helper__["b" /* MATH */].calcRadius(this.points);
    const circle = new this.circleClass(x, y, radius);
    this.circle = circle;
    this.parentElement.prepend(circle.element);
  }
  _updateCircle() {
    const { x, y } = __WEBPACK_IMPORTED_MODULE_0__helper__["b" /* MATH */].calcCenter(this.points);
    const radius = __WEBPACK_IMPORTED_MODULE_0__helper__["b" /* MATH */].calcRadius(this.points);
    this.circle.update({ x, y, radius });
  }
  _updateParallelogram() {
    this.parallelogram.update(this.points);
  }
  _updateActivePoint(x, y) {
    this.points[this.activePoint].update({ x, y });
  }
  _updateSymmetricPoint() {
    const { x, y, symmetricPoint } = __WEBPACK_IMPORTED_MODULE_0__helper__["b" /* MATH */].calcSymmetricPoint(this.points, this.activePoint);
    this.points[symmetricPoint].update({ x, y });
  }
  _findPoint(x, y) {
    const point = this.points.findIndex(point => point.x > x - __WEBPACK_IMPORTED_MODULE_0__helper__["a" /* CIRCLE */].radius && point.x < x + __WEBPACK_IMPORTED_MODULE_0__helper__["a" /* CIRCLE */].radius && point.y > y - __WEBPACK_IMPORTED_MODULE_0__helper__["a" /* CIRCLE */].radius && point.y < y + __WEBPACK_IMPORTED_MODULE_0__helper__["a" /* CIRCLE */].radius);
    return point;
  }
  get isCompleted() {
    return this.points.length === 4;
  }
}

/* harmony default export */ __webpack_exports__["a"] = (Group);

/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__helper__ = __webpack_require__(0);


class Point {
  constructor(x, y, radius) {
    this.x = x;
    this.y = y;
    this.r = radius >= 0 ? radius : null;
    this.el = null;
    this.circle = null;
    this.label = null;
    this._createElement();
  }
  _createLabel() {
    const position = this.r ? 'translate(-10 0)' : 'translate(10 10)';
    const label = document.createElementNS('http://www.w3.org/2000/svg', 'text');
    label.setAttribute('transform', position);
    label.setAttribute('fill', __WEBPACK_IMPORTED_MODULE_0__helper__["d" /* TEXT */].fill.color.default);
    this.label = label;
    this.el.appendChild(label);
  }
  _createCircle() {
    const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    const stroke = this.r === null ? __WEBPACK_IMPORTED_MODULE_0__helper__["a" /* CIRCLE */].stroke.color.point : __WEBPACK_IMPORTED_MODULE_0__helper__["a" /* CIRCLE */].stroke.color.default;
    circle.setAttribute('stroke', stroke);
    circle.setAttribute('stroke-width', __WEBPACK_IMPORTED_MODULE_0__helper__["a" /* CIRCLE */].stroke.width);
    circle.setAttribute('fill', 'url(#linearGradient)');
    this.circle = circle;
    this.el.appendChild(circle);
  }
  _createElement() {
    this.el = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this._createLabel();
    this._createCircle();
    this._updateView();
  }
  _updateView() {
    const text = this.r ? `Area: ${Math.round(this.r)}` : `(${this.x},${this.y})`;
    this.label.innerHTML = text;
    this.circle.setAttribute('r', this.r || __WEBPACK_IMPORTED_MODULE_0__helper__["a" /* CIRCLE */].radius);
    this.el.setAttribute('transform', `translate(${this.x},${this.y})`);
  }
  update({ x, y, radius }) {
    this.x = x;
    this.y = y;
    this.r = radius || null;
    this._updateView();
  }
  get element() {
    return this.el;
  }
  get positionX() {
    return this.x;
  }
  get positionY() {
    return this.y;
  }
}

/* harmony default export */ __webpack_exports__["a"] = (Point);

/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__helper__ = __webpack_require__(0);


class Parallelogram {
  constructor(points) {
    this.coord = Parallelogram.getCoord(points);
    this.el = null;
    this._createElement();
  }
  _createElement() {
    this.el = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
    this.el.setAttribute('stroke', __WEBPACK_IMPORTED_MODULE_0__helper__["c" /* PARALLELOGRAM */].stroke.color.default);
    this.el.setAttribute('stroke-width', __WEBPACK_IMPORTED_MODULE_0__helper__["c" /* PARALLELOGRAM */].stroke.width);
    this.el.setAttribute('fill', 'url(#linearGradient)');
    this._updateView();
  }
  _updateView() {
    this.el.setAttribute('points', this.coord);
  }
  update(points) {
    this.coord = Parallelogram.getCoord(points);
    this._updateView();
  }
  get element() {
    return this.el;
  }
  static getCoord(points) {
    return points.map(p => p.x + ',' + p.y).join(' ');
  }
}

/* harmony default export */ __webpack_exports__["a"] = (Parallelogram);

/***/ })
/******/ ]);